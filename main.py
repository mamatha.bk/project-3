# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from scripts.core.calculate import Calculator
obj = Calculator()
shape1 = input("Enter shape\n")
if shape1 == "circle":
    r = int(input("Enter radius"))
    ans = obj.circle(r)
    print(ans)
elif shape1 == "rectangle":
    l3 = int(input("Enter length"))
    b = int(input("Enter breadth"))
    ans = obj.rectangle(l3, b)
    print(ans)
elif shape1 == "triangle":
    b = int(input("Enter base:"))
    h = int(input("Enter height:"))
    l2 = int(input("Enter length:"))
    ans = obj.triangle(b, h, l2)
    print(ans)
elif shape1 == "square":
    l1 = int(input("Enter length:"))
    ans = obj.square(l1)
    print(ans)
