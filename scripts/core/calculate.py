import math


class Calculator:

    @staticmethod
    def circle(radius):
        area_of_circle = math.pi * (radius ** 2)
        per_of_circle = 2 * math.pi * radius
        values = (area_of_circle, per_of_circle)
        return values

    @staticmethod
    def rectangle(length, breadth):
        area_of_rect = length * breadth
        per_of_rect = 2 * (length + breadth)
        values = (area_of_rect, per_of_rect)
        return values

    @staticmethod
    def triangle(base, height, length):
        area_of_tri = 0.5 * base * height
        per_of_tri = base * height * length
        values = (area_of_tri, per_of_tri)
        return values

    @staticmethod
    def square(length):
        area_of_squ = length * length
        per_of_squ = 4 * length
        values = (area_of_squ, per_of_squ)
        return values
